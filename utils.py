from matplotlib.animation import FuncAnimation
from IPython import embed
from queue import Queue, deque
from keras import backend as K
import tensorflow as tf
from tensorflow import Session, Tensor
from typing import List, Generator, Tuple
import numpy as np
from collections import namedtuple
from image_stream import ImageStream


def list_to_queue(l : List, n_copies : int) -> Queue:
	"""
	l : List
		A python list to be loaded into the queue.

	n_copies : int
		Number of copies of l to load into queue

	Returns

	queue : Queue
		A threadsafe Queue that contains n_copies of List l.
	"""

	queue = Queue()
	queue.queue = deque(l * n_copies)

	return queue

def frame_numbers_to_queue(create_data_generator):
	def wrapper(frame_numbers, batch_size, frame_stream, ben=None, lambda_sigma=None, for_predict_generator=False):
		queue = Queue()
		queue.queue = deque(frame_numbers * 5000)

		if ben is None:
			return create_data_generator(
				queue, batch_size, frame_stream, for_predict_generator)
		else:
			return create_data_generator(queue, batch_size, 
				frame_stream, ben, lambda_sigma, for_predict_generator)

	return wrapper


def recreate_frame_stream(create_data_generator):
	def wrapper(frame_numbers, batch_size, frame_stream, for_predict_generator=False):
		init_args = frame_stream.init_args
		frame_stream = frame_stream.__class__(*init_args)

		return create_data_generator(
			frame_numbers, batch_size, frame_stream, for_predict_generator)

	return wrapper

def restart_tf_session() -> Session:
	"""
	Returns

	sess : Session
		New TensorFlow session.

	Restarts the TensorFlow session underlying Keras.
	"""
	K.get_session().close()
	sess = tf.Session()
	K.set_session(sess)

	return sess


def reduce_median(x : Tensor) -> Tensor:
	"""
	x : Tensor
		An n dimensional input tensor

	Returns

	median : Tensor
		An n-1 dimensional tensor. The result of reducing the tensor x 
		by calculating the median along the last dimension.

	Calculates the median of a TensorFlow tensor along the last dimension.
	If dim(x) > 2 the first n-1 dimensions are flattened, the median is computed,
	then the reduced array is reshaped into the original shape.
	"""
	
	shape = x.get_shape().as_list()
	last_dim_size = shape[-1]
	
	sorted_x = tf.nn.top_k(x, k=last_dim_size).values
	
	is_size_even = tf.equal( tf.mod(last_dim_size, 2) , 0 )

	even_median = lambda : 0.5 * (
		sorted_x[..., last_dim_size // 2] + sorted_x[..., last_dim_size // 2 - 1] )
	
	odd_median = lambda : sorted_x[..., last_dim_size // 2]
	
	median = tf.cond(is_size_even, even_median, odd_median)

	return median


def calc_static_background(x_hat):

	B0 = np.median(x_hat, axis=0)
	B0.shape = (1, len(B0))

	return B0


def calc_dynamic_backgrounds(x_hat, B0, lambda_sigma):
	sigma = np.mean(np.sqrt(np.abs((x_hat - B0) / lambda_sigma)), axis=0)
	true_idxs = np.where( np.abs((x_hat-B0)) <= sigma )
	B = np.repeat(B0, repeats=len(x_hat), axis=0)
	B[true_idxs] = x_hat[true_idxs]

	return B


@frame_numbers_to_queue
def create_data_generator(
	frame_numbers, 
	batch_size, 
	frame_stream, 
	for_predict_generator=False):
		"""
		frame_numbers : Queue
			Queue of frame numbers to be decompressed from ImageStream vs.
			Default is to decompress all frames of vs in order.

		batch_size : int
			Training batch_size.

		image_folder : str
			Location of image files.

		Yields
			(batch, batch) : Tuple[np.ndarray, np.ndarray]
				A tuple of identical batches of images to be used for 
				training a Keras ben.

		Returns a generator that can be used to train a TensorFlow network with
		images from a folder.
		"""

		init_args = frame_stream.init_args
		frame_stream = frame_stream.__class__(*init_args)

		while True:
			batch = []
			for _ in range(batch_size):
				i = frame_numbers.get()
				#frame = vs[i].flatten()
				frame = frame_stream[i]
				frame = np.expand_dims(frame, axis=-1)
				batch.append(frame)

			batch = np.array(batch)

			#if this generator is going to be supplied to the predict_generator 
			#method return batch. If it's going to be supplied to fit_generator
			#return (batch, batch)
			if for_predict_generator:
				yield batch
			else:
				yield (batch, batch)


def predict_on_frame_numbers(model, frame_numbers, batch_size, frame_stream):
		
	val_samples = len(frame_numbers)

	predict_gen = create_data_generator(frame_numbers, 
		batch_size = batch_size, 
		frame_stream = frame_stream,
		for_predict_generator = True)

	x_hat = model.predict_generator(predict_gen, 
		val_samples = val_samples, 
		nb_worker = 10, 
		pickle_safe = True)

	return x_hat


def eval_metrics_on_frame_numbers(model, frame_numbers, batch_size, frame_stream):
		
	val_samples = len(frame_numbers)

	train_gen = create_data_generator(frame_numbers, 
		batch_size = batch_size, 
		frame_stream = frame_stream)

	metrics = model.evaluate_generator(train_gen, 
		val_samples = val_samples, 
		nb_worker = 10, 
		pickle_safe = True)

	return metrics

def animate_list_of_imgs(lst_of_imgs, cmap, interval=25):
	fig, ax = plt.subplots()
	def update(i):
		img = lst_of_imgs[i]
		ax.cla()
		ax.imshow(img, cmap=cmap)

	ani = FuncAnimation(fig, update, range(0, len(lst_of_imgs)), interval=interval)

	return ani



if __name__ == '__main__':
	frame_numbers_queue = list_to_queue(train_frame_numbers, n_copies=2)
	predict_gen = create_predict_generator(frame_numbers_queue, 
		batch_size = batch_size, 
		video_file_name = image_folder, 
		x_chunk_idx = x_chunk_idx, 
		y_chunk_idx = y_chunk_idx, 
		total_x_chunks = total_x_chunks, 
		total_y_chunks = total_y_chunks)