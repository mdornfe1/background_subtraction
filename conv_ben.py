from IPython import embed
import tensorflow as tf
import numpy as np
from numpy.random import permutation
import matplotlib.pyplot as plt
from funcy import rpartial


from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D
from keras.regularizers import l2
from keras.metrics import mean_squared_error
from keras.objectives import binary_crossentropy

from image_stream import ImageStream
from utils import (reduce_median, frame_numbers_to_queue, 
	recreate_frame_stream, eval_metrics_on_frame_numbers, create_data_generator)
from callbacks import LossHistory, ThresholdEarlyStopping

def ben_loss(x, x_hat, lambda_sigma, batch_size, image_shape):
	"""
	Custom loss function for training background extraction networks (bens).
	"""

	#It's easier to flatten the images before finding the mean and median.

	num_pixels = np.product(image_shape)
	x = tf.reshape(x, shape=[batch_size, num_pixels])
	x_hat = tf.reshape(x_hat, shape=[batch_size, num_pixels])

	B0 = reduce_median(tf.transpose(x))
	# I divide by sigma in the next step. So I add a small float32 to F0
	# so as to prevent sigma from becoming 0 or Nan.
	
	F0 = tf.abs(x_hat - B0) + 1e-10
	
	sigma = tf.reduce_mean(tf.sqrt(F0 / lambda_sigma), axis=0)
	
	background_term = tf.reduce_mean(F0 / sigma, axis=1)
	
	regularization = lambda_sigma * tf.reduce_mean(sigma)
	
	bce = binary_crossentropy(x, x_hat)
	
	loss = bce +  background_term + regularization

	return loss

def create_encoding_layer(image_shape, num_filters, kernel_size, lambda_W):
	layers = []

	layers.append(
		Conv2D(input_shape=image_shape, kernel_size=kernel_size, 
			filters=num_filters, padding='same', activation='relu', 
			kernel_regularizer=l2(lambda_W), kernel_initializer='glorot_uniform') )

	layers.append(
		MaxPooling2D(pool_size=(2,2), padding='same') )

	return layers


def create_decoding_layer(image_shape, num_filters, kernel_size, lambda_W):
	layers = []

	layers.append(
		Conv2D(kernel_size=kernel_size, 
			filters=num_filters, padding='same', activation='relu', 
			kernel_regularizer=l2(lambda_W), kernel_initializer='glorot_uniform') )

	layers.append(
		UpSampling2D( size=(2,2) ) )

	return layers


def build_ben(lambda_W, image_shape, encoding_filters, 
	decoding_filters, num_channels, optimizer, loss, **kwargs):
	
	if len(encoding_filters) != len(decoding_filters):
		raise ValueError(
			'len(encoding_filters) must equal len(decoding_filters)')

	layers = []
	nb_row = 3
	nb_col = 3
	kernel_size = (nb_row, nb_col)
	with tf.device('/gpu:1'):

		for num_filters in encoding_filters:
			layers += (
				create_encoding_layer(
					image_shape, num_filters, kernel_size, lambda_W) )

		for num_filters in decoding_filters:
			layers += (
				create_decoding_layer(
					image_shape, num_filters, kernel_size, lambda_W) )
		
		#add in last conv layers to make sure output image has same number
		#of channels as input image.
		layers.append(
			Conv2D(kernel_size=(nb_row, nb_col), 
				filters=num_channels, padding='same', activation='sigmoid', 
				kernel_regularizer=l2(lambda_W), kernel_initializer='glorot_uniform') )

	ben = Sequential()
	for layer in layers:
		ben.add(layer)

	ben.compile(optimizer, loss, metrics=[mean_squared_error])

	return ben


def build_and_train(frame_stream, train_frame_numbers, 
	val_frame_numbers, batch_size, nb_epoch, nb_row, nb_col, num_filters,
	lambda_sigma, lambda_W, patience, p_delta_ben, optimizer):

	num_train_frames = len(train_frame_numbers)
	num_val_frames = len(val_frame_numbers)
	image_shape = (*frame_stream.shape, 1)

	#build ben
	loss_func = rpartial(ben_loss, lambda_sigma, batch_size, image_shape)
	ben = build_ben(lambda_W, image_shape, num_filters, 
		optimizer, loss_func)

	
	#Here I find initial val_mse so I can implement threshold early stopping
	#as a percentage of val_mse
	_, val_mse = eval_metrics_on_frame_numbers(ben, 
		val_frame_numbers, batch_size, frame_stream)

	#setup training data generatros
	train_gen = create_data_generator(train_frame_numbers, batch_size, frame_stream)
	val_gen = create_data_generator(val_frame_numbers, batch_size, frame_stream)

	#setup callback functions
	delta_ben = p_delta_ben * val_mse 
	loss_history = LossHistory()
	early_stopping = ThresholdEarlyStopping(monitor='val_mean_squared_error', 
		threshold=delta_ben, patience=patience, mode='below', verbose=0)

	#train
	ben.fit_generator(generator=train_gen, validation_data=val_gen, 
		nb_epoch=nb_epoch, samples_per_epoch=num_train_frames, 
		nb_val_samples=num_val_frames, callbacks=[loss_history, early_stopping], 
		nb_worker=10, pickle_safe=True)

	return ben, loss_history

if __name__ == '__main__':
	#hyper parameters
	batch_size = 5
	nb_epoch = 200
	nb_row = 3
	nb_col = 3
	num_filters = [16, 8, 4, 8, 1]
	lambda_sigma = 0.5
	lambda_W = 0.001
	patience = 1 #early stopping patience.
	p_delta_ben = 0.05 #percentage of initial val_mse at which early stopping kicks in.
	optimizer = 'adam'

	#data parameters
	image_folder = './test_data/jpegs/boats'
	frame_stream = ImageStream(image_folder, color=False)
	num_frames = len(frame_stream)
	frame_numbers = permutation( np.arange(num_frames-1) )
	train_frame_numbers = [int(i) for i in frame_numbers[:25]]
	val_frame_numbers = [int(i) for i in frame_numbers[25:]]
	num_train_frames = len(train_frame_numbers)
	num_val_frames = len(val_frame_numbers)
	image_shape = (*frame_stream.shape, 1)

	ben, loss_history = build_and_train(
		frame_stream, train_frame_numbers, val_frame_numbers, batch_size, 
		nb_epoch, nb_row, nb_col, num_filters, lambda_sigma, 
		lambda_W, patience, p_delta_ben, optimizer)

	train_gen = create_data_generator(train_frame_numbers, batch_size, frame_stream)
	x = train_gen.__next__()[0]
	x_hat = ben.predict(x)

	plt.style.use('bmh')
	fig, ax = plt.subplots()
	ax.imshow(np.squeeze(np.abs(x[0]-x_hat[0])), cmap='gray')
	ax.set_title('Convolutional Background Subtraction')

	fig, ax = plt.subplots(2, 1 , sharex=True)
	ax[0].plot(loss_history.train_loss)
	ax[1].plot(loss_history.train_mse)
	ax[1].set_xlabel('epochs')
	ax[0].set_ylabel('loss')
	ax[1].set_ylabel('mean_squared_error')


	plt.show()
	embed()

