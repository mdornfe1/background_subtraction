import shelve
from collections import namedtuple
from itertools import product
from threading import Thread
import numpy as np
from scipy.io import loadmat
from sklearn.metrics import fbeta_score
from queue import Queue, deque
from image_stream import ImageStream


def detect_foreground(x_hat, B_hat, epsilon):
	x_hat = x_hat / np.amax(x_hat)
	B_hat = B_hat / np.amax(B_hat)
	F = np.zeros_like(x_hat)
	mask = np.where(np.abs(x_hat - B_hat) > epsilon)
	F[mask] = 1

	return F

def calc_fbeta_score(params_queue, score_queue, xs, x_hats, B_hats, foreground_gts, beta):
	 
	while True:
		lambda_sigma, epsilon = params_queue.get()
		x_flattened = []
		x_hat_flattened = []
		B_hat_flattened = []
		foreground_gt_flattened = []
		for frame_number in frame_numbers:
			x = xs[frame_number]
			x_hat = x_hats[Key(frame_number, lambda_sigma)]
			B_hat = B_hats[Key(frame_number, lambda_sigma)]
			foreground_gt = foreground_gts[frame_number]

			x_flattened.append(x.flatten())
			x_hat_flattened.append(x_hat.flatten())
			B_hat_flattened.append(B_hat.flatten())
			foreground_gt_flattened.append(foreground_gt.flatten())

		x = np.hstack(x_flattened)
		x_hat = np.hstack(x_hat_flattened)
		B_hat = np.hstack(B_hat_flattened)

		foreground_gt = np.hstack(foreground_gt_flattened)
		foreground_pred = detect_foreground(x, B_hat, epsilon)

		score = fbeta_score(foreground_gt.astype(bool), foreground_pred.astype(bool), beta=beta)

		score_queue.put((lambda_sigma, epsilon, score))


if __name__ == '__main__':
	image_folder = './test_data/jpegs/boats'
	frame_stream = ImageStream(image_folder, color=False)
	gt_file = './test_data/gt/boats_GT.mat'
	foreground_gts = np.rollaxis(loadmat(gt_file)['GT'], axis=-1)
	num_frames = len(frame_stream)
	frame_numbers = range(num_frames-1)
	Key = namedtuple('Key', ['frame_number', 'lambda_sigma'])

	with shelve.open('./data.db') as db:
		xs = db['xs']
		x_hats = db['x_hats']
		B_hats = db['B_hats']

	lambda_sigmas = np.arange(0.1, 1, 0.1)
	epsilons = np.arange(0, 1, 0.01)

	params_queue = Queue()
	score_queue = Queue()
	params_queue.queue = deque(product(lambda_sigmas, epsilons))

	num_cores = 40
	beta = 3.0
	for core in range(num_cores):
		worker = Thread(target=calc_fbeta_score, args=(params_queue, score_queue, x, x_hats, B_hats, foreground_gts, beta))
		worker.setDaemon(True)
		worker.start()
	
	params_queue.join()

	scores = list(score_queue.queue)
	scores.sort(key=lambda tup: tup[2])
	best_lambda_sigma, best_epsilon, best_score = scores[-1]

	foregrounds = []
	for frame_number in np.sort(frame_numbers):
		x = xs[frame_number]
		x_hat = x_hats[Key(frame_number, best_lambda_sigma)]
		B_hat = B_hats[Key(frame_number, best_lambda_sigma)]
		foreground = detect_foreground(x, B_hat, best_epsilon)
		foregrounds.append(foreground)

	animate_list_of_imgs(foregrounds, 'gray')