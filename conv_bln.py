from IPython import embed
import tensorflow as tf
import numpy as np
from numpy.random import permutation
import matplotlib.pyplot as plt
from funcy import rpartial

from keras.models import model_from_json
from keras.objectives import binary_crossentropy
from keras.metrics import mean_squared_error

import conv_ben
from image_stream import ImageStream
from utils import (reduce_median, frame_numbers_to_queue, 
	eval_metrics_on_frame_numbers, create_data_generator, list_to_queue)
from callbacks import LossHistory, ThresholdEarlyStopping
from global_vars import TESLA, QUADRO



def calc_dynamic_backgrounds(x_hat, B0, lambda_sigma):
	sigma = np.mean(np.sqrt(np.abs((x_hat - B0) / lambda_sigma)), axis=0)
	true_idxs = np.where( np.abs((x_hat-B0)) <= sigma )
	B0 = np.expand_dims(B0, axis=0)
	B = np.repeat(B0, repeats=len(x_hat), axis=0)
	B[true_idxs] = x_hat[true_idxs]

	return B


@frame_numbers_to_queue
def create_bln_data_generator(
	frame_numbers, 
	batch_size, 
	frame_stream,
	ben,
	lambda_sigma, 
	for_predict_generator=False):

	init_args = frame_stream.init_args
	frame_stream = frame_stream.__class__(*init_args)

	while True:
		x_batch = []
		for _ in range(batch_size):
			i = frame_numbers.get()
			frame = frame_stream[i]
			frame = np.expand_dims(frame, axis=-1)
			x_batch.append(frame)

		x_batch = np.array(x_batch)
		x_hat_batch = ben.predict_on_batch(x_batch)
		B0 = np.median(x_hat_batch, axis=0)
		B_batch = calc_dynamic_backgrounds(x_hat_batch, B0, lambda_sigma)


		#if this generator is going to be supplied to the predict_generator 
		#method return batch. If it's going to be supplied to fit_generator
		#return (batch, batch)
		if for_predict_generator:
			yield B_batch
		else:
			yield (B_batch, B_batch)

def load_frames(frame_stream, frame_numbers):
	frames = []
	for n in frame_numbers:
		frame = frame_stream[n]
		frame = np.expand_dims(frame, axis=0)
		frame = np.expand_dims(frame, axis=-1)
		frames.append(frame)

	frames = np.vstack(frames)

	return frames

def S(x_hat, batch_size, lambda_sigma):
	x_hat_batches = np.split(x_hat, batch_size, axis=0)
	B = []
	for x_hat_batch in x_hat_batches:
		B0 = np.median(x_hat_batch, axis=0)
		B_batch = calc_dynamic_backgrounds(x_hat_batch, B0, lambda_sigma)
		B.append(B_batch)

	B = np.vstack(B)

	return B

def build_bln(ben, optimizer):
	ben_architecture = ben.to_json()
	ben_weights = ben.get_weights()

	with tf.device(TESLA):
		bln = model_from_json(ben_architecture)
		bln.set_weights(ben_weights)
		bln.compile(
			optimizer=optimizer, 
			loss=binary_crossentropy,
			 metrics=[mean_squared_error])

	return bln


def build_and_train(ben, frame_stream, train_frame_numbers, 
	val_frame_numbers, batch_size, nb_epoch, lambda_sigma, patience, p_delta_bln, 
	optimizer):

	ben_architecture = ben.to_json()
	ben_weights = ben.get_weights()

	with tf.device(TESLA):
		bln = model_from_json(ben_architecture)
		bln.set_weights(ben_weights)
		bln.compile(
			optimizer=optimizer, 
			loss=binary_crossentropy,
			 metrics=[mean_squared_error])

	#load and transform data
	x_train = load_frames(frame_stream, train_frame_numbers)
	x_val = load_frames(frame_stream, val_frame_numbers)

	x_hat_train = ben.predict(x_train)
	x_hat_val = ben.predict(x_val)

	B_train = S(x_hat_train, batch_size, lambda_sigma)
	B_val = S(x_hat_val, batch_size, lambda_sigma)

	#Here I find initial val_mse so I can implement threshold early stopping
	#as a percentage of val_ms
	_, val_mse = bln.evaluate(B_val, B_val, batch_size)

	#setup callback functions
	delta_bln = p_delta_bln * val_mse 
	loss_history = LossHistory()
	early_stopping = ThresholdEarlyStopping(monitor='val_mean_squared_error', 
		threshold=delta_bln, patience=patience, mode='below', verbose=0)

	bln.fit(x=B_train, y=B_train, batch_size=batch_size, 
		nb_epoch=nb_epoch, validation_data=(B_val, B_val),
		shuffle=True)

	return bln, loss_history


if __name__ == '__main__':
		#hyper parameters
	batch_size = 5
	nb_epoch = 200
	nb_row = 3
	nb_col = 3
	num_filters = [16, 8, 4, 8, 1]
	lambda_sigma = 0.5
	lambda_W = 0.001
	patience = 1 #early stopping patience.
	p_delta_ben = 0.05 #percentage of initial val_mse at which early stopping kicks in.
	p_delta_bln = 0.1
	optimizer = 'adam'

	#data parameters
	image_folder = './test_data/jpegs/boats'
	frame_stream = ImageStream(image_folder, color=False)
	num_frames = len(frame_stream)
	frame_numbers = permutation( np.arange(num_frames-1) )
	train_frame_numbers = [int(i) for i in frame_numbers[:25]]
	val_frame_numbers = [int(i) for i in frame_numbers[25:]]
	num_train_frames = len(train_frame_numbers)
	num_val_frames = len(val_frame_numbers)
	image_shape = (*frame_stream.shape, 1)

	ben, ben_loss_history = conv_ben.build_and_train(frame_stream, train_frame_numbers, 
	val_frame_numbers, batch_size, nb_epoch, nb_row, nb_col, num_filters,
	lambda_sigma, lambda_W, patience, p_delta_ben, optimizer)

	bln, bln_loss_history = build_and_train(ben, frame_stream, train_frame_numbers, 
	val_frame_numbers, nb_epoch, lambda_sigma, patience, p_delta_bln, 
	optimizer)

	embed()

	train_gen = create_data_generator(train_frame_numbers, batch_size, frame_stream)
	x = train_gen.__next__()[0]
	x_hat = ben.predict(x)
	B0 = np.median(x_hat, axis=0)
	B = calc_dynamic_backgrounds(x_hat, B0, lambda_sigma)
	B_hat = bln.predict(B)
	imshow(np.squeeze(np.abs(x[0]-B_hat[0])>0.2),cmap='gray')
