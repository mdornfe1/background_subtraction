import shelve
from collections import namedtuple
from funcy import partial, rpartial
from itertools import product, permutations as permutation_gen
from IPython import embed
import numpy as np
from numpy.random import permutation
from scipy.io import loadmat
from sklearn.metrics import fbeta_score

import conv_ben
import conv_bln
from image_stream import ImageStream, load_frames
from callbacks import LossHistory, ThresholdEarlyStopping
from cross_validation import BenGridSearchCV, BenRandomSearchCV

Key = namedtuple('Key', ['frame_number', 'lambda_sigma'])

def detect_foreground(x_hat, B_hat, epsilon):
	x_hat = x_hat / np.amax(x_hat)
	B_hat = B_hat / np.amax(B_hat)
	F = np.zeros_like(x_hat)
	mask = np.where(np.abs(x_hat - B_hat) > epsilon)
	F[mask] = 1

	return F

def build_ben_and_loss(lambda_W, lambda_sigma, image_shape, encoding_filters,
	decoding_filters, num_channels, batch_size, optimizer, **kwargs):

	loss = rpartial(conv_ben.ben_loss, lambda_sigma, batch_size, image_shape)
	ben = conv_ben.build_ben(lambda_W, image_shape, encoding_filters, 
		decoding_filters, num_channels, optimizer, loss)

	return ben


def permutation_list(lst, n):
	return list(permutation_gen(lst, n))


def calc_dynamic_backgrounds(frame_stream, train_frame_numbers, val_frame_numbers, lambda_sigmas):
	num_train_frames = len(train_frame_numbers)
	num_val_frames = len(val_frame_numbers)
	image_shape = (*frame_stream.shape, 1)

	x_train = load_frames(train_frame_numbers, frame_stream)
	x_val = load_frames(val_frame_numbers, frame_stream)

	x_hats = {}
	B_hats = {}
	
	nb_epoch = 200
	batch_size = 5

	for lambda_sigma in lambda_sigmas:
			
		params_grid = dict(
			lambda_W = np.arange(1e-3-1e-4, 1e-3+1e-4, 1e-4),
			lambda_sigma = [lambda_sigma], 
			image_shape = [image_shape], 
			encoding_filters = permutation_list(range(16), 2),
			decoding_filters = permutation_list(range(16), 2),
			num_channels = [1],
			optimizer = ['adam'], 
			batch_size = [5],
			p_delta_ben = [0.05],
			patience = [1])

		random_search = BenRandomSearchCV(build_ben_and_loss, params_grid)

		#train ben
		ben, params, ben_loss_history = random_search.fit(
			x_train=x_train, y_train=x_train, batch_size=batch_size, num_iters=50, 
			nb_epoch=nb_epoch, x_val=x_val, y_val=x_val, shuffle=True)

		#prepare training data for bln network
		x_hat_train = ben.predict(x_train)
		x_hat_val = ben.predict(x_val)
		B_train = conv_bln.S(x_hat_train, batch_size, lambda_sigma)
		B_val = conv_bln.S(x_hat_val, batch_size, lambda_sigma)

		#setup bln network
		bln = conv_bln.build_bln(ben, optimizer)

		#setup callback functions for bln
		_, val_mse = bln.evaluate(B_val, B_val, batch_size)
		delta_bln = p_delta_bln * val_mse 
		bln_loss_history = LossHistory()
		early_stopping = ThresholdEarlyStopping(monitor='val_mean_squared_error', 
			threshold=delta_bln, patience=patience, mode='below', verbose=0)

		#train bln
		bln.fit(x=B_train, y=B_train, batch_size=batch_size, 
		nb_epoch=nb_epoch, validation_data=(B_val, B_val),
		shuffle=True, callbacks=[bln_loss_history, early_stopping])

		#predict output of bln
		B_hat_train = bln.predict(B_train)
		B_hat_val = bln.predict(B_val)

		#store outputs of networks in dicts
		for x_hat, frame_number in zip(x_hat_train, train_frame_numbers):
			x_hats[Key(frame_number, lambda_sigma)] = np.squeeze(x_hat)

		for x_hat, frame_number in zip(x_hat_val, val_frame_numbers):
			x_hats[Key(frame_number, lambda_sigma)] = np.squeeze(x_hat)

		for B_hat, frame_number in zip(B_hat_train, train_frame_numbers):
			B_hats[Key(frame_number, lambda_sigma)] = np.squeeze(B_hat)

		for B_hat, frame_number in zip(B_hat_val, val_frame_numbers):
			B_hats[Key(frame_number, lambda_sigma)] = np.squeeze(B_hat)

		K.clear_session()

	return x_hats, B_hats


if __name__ == '__main__':
	#data parameters
	image_folder = './test_data/jpegs/boats'
	frame_stream = ImageStream(image_folder, color=False)
	num_frames = len(frame_stream)
	frame_numbers = permutation( np.arange(num_frames-1) )
	train_frame_numbers = [int(i) for i in frame_numbers[:25]]
	val_frame_numbers = [int(i) for i in frame_numbers[25:]]

	#foreground truth data
	gt_file = './test_data/gt/boats_GT.mat'
	foreground_gts = np.rollaxis(loadmat(gt_file)['GT'], axis=-1)

	lambda_sigmas = np.arange(0.1, 1, 0.1)
	x_hats, B_hats = calc_dynamic_backgrounds(frame_stream, train_frame_numbers, val_frame_numbers, lambda_sigmas)
	embed()

	with shelve.open('./data.db') as db:
		db['xs'] = x
		db['x_hats'] = x_hats
		db['B_hats'] = B_hats

	#detect foreground
	best_score = 0	
	best_lambda_sigma = None	
	best_epsilon = None
	best_beta = None	
	for lambda_sigma in lambda_sigmas:
		x_hat_flattened = []
		B_hat_flattened = []
		foreground_gt_flattened = []
		for frame_number in train_frame_numbers + val_frame_numbers:
			x_hat = x_hats[Key(frame_number, lambda_sigma)]
			B_hat = B_hats[Key(frame_number, lambda_sigma)]
			foreground_gt = foreground_gts[frame_number]

			x_hat_flattened.append(x_hat.flatten())
			B_hat_flattened.append(B_hat.flatten())
			foreground_gt_flattened.append(foreground_gt.flatten())

		x_hat = np.hstack(x_hat_flattened)
		B_hat = np.hstack(B_hat_flattened)
		foreground_gt = np.hstack(foreground_gt_flattened)

		for epsilon in epsilons:
				foreground_pred = detect_foreground(x_hat, B_hat, epsilon)

				score = fbeta_score(foreground_gt.astype(bool), foreground_pred.astype(bool), beta=0.1)

				if score > best_score:
					best_score = score
					best_lambda_sigma = lambda_sigma
					best_epsilon = epsilon

	foregrounds = []
	for frame_number in np.sort(frame_numbers):
		x_hat = x_hats[Key(frame_number, best_lambda_sigma)]
		B_hat = B_hats[Key(frame_number, best_lambda_sigma)]
		foreground = detect_foreground(x_hat, B_hat, best_epsilon)
		foregrounds.append(foreground)










	embed()

