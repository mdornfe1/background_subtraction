import numpy as np

def detect_foreground(x_hat, B_hat, epsilon):
	F = np.zeros_like(x_hat)
	mask = np.where(np.abs(x_hat - B_hat) > epsilon)
	F[mask] = 1

	return F

epsilons = np.arange(0, 1, 0.01)
epsilon = 0.5
for lambda_sigma in lambda_sigmas:
	for frame_number in val_frame_numbers:
		x_hat = x_hats[Key(None, None, frame_number, None)]
		B_hat = B_hats[Key(None, None, frame_number, lambda_sigma)]
		F = detect_foreground(x_hat, B_hat, epsilon)