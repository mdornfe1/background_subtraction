from skimage import color
import numpy as np
import matplotlib.pyplot as plt
import os, sys
import re
from scipy.misc import imread
from scipy.io import loadmat
from threading import Thread
from multiprocessing import cpu_count
from queue import Queue, deque

plt.style.use('bmh')

class FrameStream:
	"""
	Parent class for implemented ImageStream and VideoStream.
	Do not use on its own.
	"""
	def __init__(self, data_source, init_args, num_frames, image_shape, color):
		self.data_source = data_source
		self.frame_number = 0
		self.num_frames = num_frames
		self.color = color
		self.shape = image_shape
		self.init_args = init_args


	def __len__(self):
		"""
		Returns number of images/frames in FrameStream Object.
		"""
		return self.num_frames

	def __getitem__(self, frame_number):
		"""
		Returns single frame if frame_number is int or a list of frames if 
		frame_number is a slice.
		"""
		if isinstance(frame_number, slice):
			indices = range(*frame_number.indices(len(self)))
			return (self._get_frame(i) for i in indices)
		else:
			return self._get_frame(frame_number)

	def __next__(self):
		if self.frame_number >= len(self):
			self.frame_number = 0
			raise StopIteration 
		else:
			self.frame_number += 1
			return self.__getitem__(self.frame_number-1)

	def __iter__(self):
		return self

	def read(self):
		return self.__next__()

	def reinitialize(self):
		"""
		Returns
			Reinitialized version of FrameStream. Useful for when
			FrameStream is passed to a multithread function.
		"""
		return self._reinitialize(self.init_args)

	@classmethod 
	def _reinitialize(cls, init_args):
		return cls(*init_args)


class ImageStream(FrameStream):
	def __init__(self, folder_name, color=True):
		"""
		folder_name : str
			Folder containing images
		color : bool
			Pass true for ImageStream to return color images. Pass false
			for ImageStream to return b+w images. Defaults to True.
		"""
		self.image_files = self._get_files_in_folder(folder_name)
		
		regex = re.compile(r'(\d+)')
		self.image_files.sort(key = lambda s: int(regex.findall(s)[0]))
		
		if color:
			frame = imread(self.image_files[0], mode='RGB') / 255
		else:
			frame = imread(self.image_files[0], mode='L') / 255

		super().__init__(data_source = folder_name, 
			init_args = (folder_name, color), 
			num_frames = len(self.image_files), 
			image_shape = frame.shape, 
			color = color)

	def _get_frame(self, frame_number):
		"""
		frame_number : int
			Number of the frame in the video file to be decompressed and
			returned.

		Returns
			frame : np.ndarray
				This is an array representing the image of the video file
				at that frame. The array frame will have dimensions
				(y_resolution, x_resolution, 3) if self.color is True
				or (y_resolution, x_resolution, 1) if self.color is False.
		"""

		image_file = self.image_files[frame_number]

		if self.color:
			frame = imread(image_file, mode='RGB')
		else:
			frame = imread(image_file, mode='L') / 255

		return frame


	def _get_files_in_folder(self, folder):
		"""
		folder : str
			folder path
		
		Returns
			full_file_paths : List[str]
				A list of full file paths of the files in the folder.

		"""
		files = os.listdir(folder)
		full_file_paths = []
		for file in files:
			full_file_path = os.path.join(folder, file)
			full_file_paths.append(full_file_path)

		return full_file_paths


	def set_color(self, color):
		"""
		color : bool
			Pass true for ImageStream to return color images. Pass false
			for ImageStream to return b+w images.
		"""
		self.color = color
		self.shape = self._get_frame(0).shape


def decompress_worker(frame_numbers_queue, frames_queue, frame_stream):
	
	#ffmpeg doesn't like it when you use the same frame_stream object
	#to decompress multiple images from a movie in different threads.
	#So here each worker creates its own copy of frame_stream.
	frame_stream = frame_stream.reinitialize()
	
	while frame_numbers_queue.qsize() > 0:
		frame_number = frame_numbers_queue.get()
		frame = frame_stream[frame_number]
		#tensorflow expects data in the shape of 
		#(batch_size, *image_shape, num_channels).
		#So here I reshape frame to be ready for 
		#training tensorflow.
		frame = np.expand_dims(frame, axis=0)
		frame = np.expand_dims(frame, axis=-1)
		frames_queue.put((frame, frame_number))


def load_frames(frame_numbers, frame_stream):
	frame_numbers_queue = Queue()
	frames_queue = Queue()
	frame_numbers_queue.queue = deque(frame_numbers)
	num_cores = cpu_count()
	for core in range(num_cores):
		worker = Thread(target=decompress_worker, args=(frame_numbers_queue, frames_queue, frame_stream))
		worker.setDaemon(True)
		worker.start()

	frame_numbers_queue.join()
	frames = list(frames_queue.queue)
	
	#sort frames so they are in the same order as frame_numbers
	frames.sort(key=lambda tup:tup[1])
	frames = [frame[0] for frame in frames]
	frames = np.vstack(frames)
	
	return frames


if __name__ == '__main__':
	folder = './test_data/jpegs/boats'
	gt_file = './test_data/gt/boats_GT.mat'
	frame_stream = ImageStream(folder, color=False)
	frame_numbers = list(range(len(frame_stream)))
	frames = load_frames(frame_numbers, frame_stream)
