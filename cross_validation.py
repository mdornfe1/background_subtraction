from random import shuffle as shuffle_list
from itertools import product
import numpy as np
from callbacks import LossHistory, ThresholdEarlyStopping
from keras import backend as K


class SearchCV:
	"""
	Parent class for GridSearchCV and RandomSearchCV.
	Don't use on its own.
	"""
	def __init__(self, 
		build_fn, 
		params_grid={}):
		"""
		build_fn : Callable[params] -> keras.models.Model
		A function that accepts neural network hyper parameters
		and returns a Keras neural network model.

		params_grid : dict[List]
		A dict in which they keys are the names of the hyper parameters
		and the values are lists of the possible values of those parameters. 
		"""

		self.build_fn = build_fn
		self.params_grid_gen = self._create_params_grid_gen(params_grid)
		self.params_loss = []

	def _create_params_grid_gen(self, params_grid):
		"""
		params_grid : dict[Iterable]

		returns

		params_grid_gen : Generator[dict]

		Returns a generator that pops off the cartesian product of the
		iterables in params_grid. params_grid_gen can be iterated over
		do to a grid search or a random search of the hyper parameter space
		of the a keras model.
		"""
		return (dict(zip(params_grid, x)) 
			for x in product(*params_grid.values()))

	def _fit(self, params, loss):
		self.params_loss.append((params, loss))
		K.clear_session()



class GridSearchCV(SearchCV):

	def fit(self, x_train, y_train, x_val, y_val, nb_epoch, 
		batch_size, callbacks=[], shuffle=True):
		"""
		x_train : Iterable

		y_train : Iterable

		x_val : Iterable

		y_val : Iterable

		nb_epoch : int

		batch_size : int

		callbacks : List

		shuffle : bool
		"""

		best_loss = np.inf
		for params in params_grid_gen:
			model = self.build_fn(**params)

			if batch_size in params:
				batch_size = params['batch_size']

			loss_history = LossHistory()
			callbacks += [early_stopping, loss_history]

			model.fit(x=x_train, y=x_train, batch_size=batch_size, 
				nb_epoch=nb_epoch, validation_data=(x_val, x_val),
				shuffle=shuffle, callbacks=callbacks)

			loss =  model.evaluate(x_val, x_val, batch_size, verbose=0)[0]

			super()._fit(params, loss)
			if loss < best_loss:
				best_loss = loss
				best_loss_history = loss_history
				best_model = model
				best_params = params

		return best_model, best_params, best_loss_history


class RandomSearchCV(SearchCV):

	def fit(self, x_train, y_train, x_val, y_val, nb_epoch, 
		batch_size, num_iters, callbacks=[], shuffle=True):
		"""
		x_train : Iterable

		y_train : Iterable

		x_val : Iterable

		y_val : Iterable

		nb_epoch : int

		batch_size : int

		num_iters : int

		callbacks : List

		shuffle : bool
		"""
		params_randomized = list(self.params_grid_gen)
		shuffle_list(params_randomized)

		best_loss = np.inf
		for _ in range(num_iters):
			params = params_randomized.pop()
			model = self.build_fn(**params)

			if batch_size in params:
				batch_size = params['batch_size']

			loss_history = LossHistory()
			callbacks += [early_stopping, loss_history]

			model.fit(x=x_train, y=x_train, batch_size=batch_size, 
				nb_epoch=nb_epoch, validation_data=(x_val, x_val),
				shuffle=shuffle, callbacks=callbacks)

			loss =  model.evaluate(x_val, x_val, batch_size, verbose=0)[0]

			super()._fit(params, loss)
			if loss < best_loss:
				best_loss = loss
				best_loss_history = loss_history
				best_model = model
				best_params = params


		return best_model, best_params, best_loss_history

class BenGridSearchCV(SearchCV):

	def fit(self, x_train, y_train, x_val, y_val, nb_epoch, 
		batch_size, callbacks=[], shuffle=True):
		"""
		x_train : Iterable

		y_train : Iterable

		x_val : Iterable

		y_val : Iterable

		nb_epoch : int

		batch_size : int

		callbacks : List

		shuffle : bool
		"""

		best_loss = np.inf
		for params in params_grid_gen:
			model = self.build_fn(**params)

			if batch_size in params:
				batch_size = params['batch_size']

			#setup ben earlystopping and loss_history
			p_delta_ben = params['p_delta_ben']
			patience = params['patience']
			_, val_mse = model.evaluate(x_val, x_val, batch_size)
			delta_ben = p_delta_ben * val_mse 
			early_stopping = ThresholdEarlyStopping(monitor='val_mean_squared_error', 
				threshold=delta_ben, patience=patience, mode='below', verbose=0)
			loss_history = LossHistory()
			callbacks += [early_stopping, loss_history]

			model.fit(x=x_train, y=x_train, batch_size=batch_size, 
				epochs=nb_epoch, validation_data=(x_val, x_val),
				shuffle=shuffle, callbacks=callbacks)

			val_loss, val_mse = model.evaluate(x_val, x_val, batch_size, verbose=0)

			super()._fit(params, [val_loss, val_mse])
			if val_loss < best_loss:
				best_loss = val_loss
				best_loss_history = loss_history
				best_model = model
				best_params = params


		return best_model, best_params, best_loss_history


class BenRandomSearchCV(SearchCV):

	def fit(self, x_train, y_train, x_val, y_val, nb_epoch, 
		batch_size, num_iters, callbacks=[], shuffle=True):
		"""
		x_train : Iterable

		y_train : Iterable

		x_val : Iterable

		y_val : Iterable

		nb_epoch : int

		batch_size : int

		num_iters : int

		callbacks : List

		shuffle : bool
		"""

		params_randomized = list(self.params_grid_gen)
		shuffle_list(params_randomized)

		best_loss = np.inf
		for _ in range(num_iters):
			params = params_randomized.pop()
			model = self.build_fn(**params)

			if batch_size in params:
				batch_size = params['batch_size']

			#setup ben earlystopping and loss_history
			p_delta_ben = params['p_delta_ben']
			patience = params['patience']
			_, val_mse = model.evaluate(x_val, x_val, batch_size)
			delta_ben = p_delta_ben * val_mse 
			early_stopping = ThresholdEarlyStopping(monitor='val_mean_squared_error', 
				threshold=delta_ben, patience=patience, mode='below', verbose=0)
			loss_history = LossHistory()
			callbacks += [early_stopping, loss_history]

			model.fit(x=x_train, y=x_train, batch_size=batch_size, 
				epochs=nb_epoch, validation_data=(x_val, x_val),
				shuffle=shuffle, callbacks=callbacks)

			val_loss, val_mse = model.evaluate(x_val, x_val, batch_size, verbose=0)

			super()._fit(params, [val_loss, val_mse])
			if val_loss < best_loss:
				best_loss = val_loss
				best_loss_history = loss_history
				best_model = model
				best_params = params


		return best_model, best_params, best_loss_history